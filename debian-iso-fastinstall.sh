#!/usr/bin/env bash

set -e # errexit
#set -f # noglob
set +o braceexpand # disable braceexpand

if [ ! "$2" ]; then
  echo "Debian fast-install ISO conversion script"
  echo "This script patches official Debian ISO image to make it install faster."
  echo
  echo "Usage: $0 <input iso image> <output iso image>"
  exit 1
fi

function check_requirements() {
    local reqs=(bsdtar wget grep sed apt-ftparchive md5sum xorriso dd)

    for req in ${reqs[*]}; do
        if ! command -v "$req" > /dev/null;
        then
            echo "ERROR: $req is required but not found"
            exit 2
        fi
    done
}
check_requirements

ISO="$1"
ISOOUT="$2"

if [ ! -r "$ISO" ]; then
  echo "ERROR: iso file $ISO does not exist or is not readable!"
  exit 3
fi

if [ -d "cd" ]; then
  echo "ERROR: "\""cd"\"" directory is already exist! Please remove it manually before running this script."
  exit 4
fi

mkdir cd
echo "Unpacking ISO, this might take some time."
bsdtar -C cd/ -xf "$ISO"

if [ ! -f "cd/.disk/info" ]; then
  echo "ERROR: .disk/info file is not found! Is that a Debian ISO?"
  exit 5
fi

# Defaults
DISTRO="buster"
ARCH="amd64"
REBUILD_INDEX=""

#DISKINFO="$(cat cd/.disk/info)"
#[[ "$DISKINFO" =~ (Debian GNU/Linux [^ ]* ([^ ]*) - [^ ]* ([^ ]*) ) ]]
#if [[ ! ${BASH_REMATCH[2],,} ]]; then
#  [[ "$DISKINFO" =~ (Official Debian GNU/Linux [^ ]* ([^ ]*) - [^ ]* ([^ ]*) ) ]]
#fi
#DISTRO="${BASH_REMATCH[2],,}"
#ARCH="${BASH_REMATCH[3],,}"

## Some ISOs have distro code name in quotes. Strip them.
#[[ "$DISTRO" =~ (\"(.*)\") ]]
#if [[ ${BASH_REMATCH[2]} ]]; then
#  DISTRO="${BASH_REMATCH[2]}"
#fi

DISTRO="$(ls -1p cd/dists/ | grep -F '/' | sed 's~/~~')"
ARCH="$(ls -1d cd/dists/$DISTRO/main/binary-* | sed 's~.*/binary-\(.*\)~\1~')"

echo "Detected Debian $DISTRO ISO, $ARCH architecture."
chmod -R +w cd

# Check for eatmydata and libeatmydata, download if needed.
# Do NOT add quotes for the file name here, it will break globbing.
# Also do not use two braces.
if [ ! -f cd/pool/main/libe/libeatmydata/eatmydata_*.deb ]; then
  echo "Downloading eatmydata DEB file"
  EATMYDATA_URL="$(wget -O - https://packages.debian.org/$DISTRO/all/eatmydata/download | grep -Po -m1 'http://.*/libeatmydata/eatmydata.*?\.deb')"
  (cd cd/pool/main/libe/libeatmydata/ && wget "$EATMYDATA_URL")
  REBUILD_INDEX=1
fi
if [ ! -f cd/pool/main/libe/libeatmydata/libeatmydata*.deb ]; then
  echo "Downloading libeatmydata DEB file"
  LIBEATMYDATA_URL="$(wget -O - https://packages.debian.org/$DISTRO/$ARCH/libeatmydata1/download | grep -Po -m1 'http://.*/libeatmydata/libeatmydata.*?\.deb')"
  (cd cd/pool/main/libe/libeatmydata/ && wget "$LIBEATMYDATA_URL")
  REBUILD_INDEX=1
fi

# Patch boot menus
if [[ "$(cat cd/.disk/cd_type)" == "live" ]]; then
  sed -i 's~\(linux  /.*\)~\1 partman/early_command="/cdrom/fast-install.sh"~g' cd/boot/grub/grub.cfg
  for i in cd/isolinux/*.cfg; do
    # For live builds
    sed -i 's~\(.*APPEND initrd.*\)~\1 partman/early_command="/cdrom/fast-install.sh"~g' "$i"
  done
else
  sed -i 's~\(linux.*\) ---~\1 partman/early_command="/cdrom/fast-install.sh" ---~g' cd/boot/grub/grub.cfg
  for i in cd/isolinux/*.cfg; do
    # For classic builds
    sed -i 's~\(append.*\) ---~\1 partman/early_command="/cdrom/fast-install.sh" ---~g' "$i"
  done
fi

# Copy fast-install script
cp files/fast-install.sh cd/fast-install.sh
chmod +x cd/fast-install.sh

if [[ "$REBUILD_INDEX" ]]; then
    # Download indices
    echo "Checking for indeces, will download files if missing."
    mkdir indices &>/dev/null || true
    INDICES="override.$DISTRO.main override.$DISTRO.main.debian-installer override.$DISTRO.contrib
                    override.$DISTRO.non-free override.$DISTRO.non-free.debian-installer"
    for indice in $INDICES; do
    if [ ! -f "indices/$indice" ]; then
        wget -O - "https://deb.debian.org/debian/indices/$indice.gz" | zcat > "indices/$indice"
    fi
    done

    # Regenerate package lists
    cp files/config-deb-template config-deb
    sed -i "s~ARCH~$ARCH~g" config-deb
    sed -i "s~DISTRO~$DISTRO~g" config-deb
    echo "Generating list of packages. You may see errors here. All errors except for "\""main"\"" are safe to ignore."
    apt-ftparchive generate config-deb
    sed -i '/MD5Sum:/,$d' cd/dists/$DISTRO/Release
    apt-ftparchive release cd/dists/$DISTRO >> cd/dists/$DISTRO/Release
    (cd cd; md5sum `find ! -name "md5sum.txt" ! -path "./isolinux/*" -follow -type f` > md5sum.txt)
else
    echo "No need to rebuild index, continue"
fi

#chmod -R -w  cd

# Get original ISO label
ISONAME="Debian"
if [ -f "cd/.disk/mkisofs" ]; then
  ISOMKISOFS="$(cat cd/.disk/mkisofs)"
  if [[ "$ISOMKISOFS" =~ (xorriso .* -V \'([^\']*)\' ) ]]; then
    ISONAME="${BASH_REMATCH[2]}"
  fi
fi

# Cut isohdpfx.bin
dd if="$ISO" bs=1 count=432 of=isohdpfx.bin

# Build ISO
xorriso -as mkisofs -V "$ISONAME" -o "$ISOOUT" -J -joliet-long -cache-inodes -isohybrid-mbr isohdpfx.bin -b isolinux/isolinux.bin -c isolinux/boot.cat -boot-load-size 4 -boot-info-table -no-emul-boot -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat cd

echo "All done."
