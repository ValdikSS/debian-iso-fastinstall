# Debian ISO FastInstall

-----

**This script patches official Debian ISO image to make it install faster.**

Debian installer software writes data with force syncronization of each written file to disk. This is a default feature of `apt/dpkg`, to ensure system state and consistency in case of sudden power loss during package manipulation.  
However this feature makes ISO installer insanely slower than it should be, and it's pointless for the OS installation stage.

**Patched ISO archive: [FTP and BitTorrent download](ftp://serv.valdikss.org.ru/Downloads/Linux ISOs/Debian ISO FastInstall/).**

### Why

Installation of Debian 10.6.0 (`debian-10.6.0-amd64-DVD-1.iso`) on a HDD inside a VM. Write caching disabled, no network access.

* Original ISO file: **1 hour 44 minutes 20 seconds**
* Patched ISO file: **10 minutes 37 seconds**

### What

The script:

* Adds `eatmydata` syncronization skipping package and corresponding library into ISO file
* Patches `debootstrap`, which is used for initial base packages installation, to use `eatmydata`
* Ensures `eatmydata` to be used during second installation stage
* Patches boot menu to load `eatmydata-udeb` in the installation

### Compatibility

Debian ISO FastInstall has been tested with official `Stretch`, `Buster`, `Bullseye` CD and DVD  regular and live images, as well as with unofficial non-free firmware ISO files.

The script speeds up only classic Debian Installer (`d-i`), but not Calamares installer found on live images *inside the OS* (classic installer is also present in live images, the script works with it).

![Debian installer types](files/debian-installer-types.png)

### Caveats

`eatmydata` and `libeatmydata1` end up being installed into the system on classic (non-live) media. You can safely remove the packages or ignore it.

### History

Slow installation of Debian images has been known since [at least 2007](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=411814).  
In 2014, Petter Reinholdtsen [implemented eatmydata-udeb](http://people.skolelinux.org/pere/blog/Speeding_up_the_Debian_installer_using_eatmydata_and_dpkg_divert.html) for Debian Edu (Skolelinux), a wrapper script which adds eatmydata usage into apt on installation. Despite the script being included into installation media even today, it was never used by default and required preseed file or custom kernel parameter.  
This script is an attempt to provide faster installation for existing iso files right now, to be used until proper upstream solution introduced.

Debian developers have included eatmydata into ISO file by my request in Debian 11 release, however no installer changes were integrated in a fear to break anything before the major release.

Debian bug reports:

* [#700633 Debootstrap is very slow. Please use eatmydata to fix this.](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=700633)
* [#984791 eatmydata-udeb: udeb included in Debian ISO, but eatmydata and libeatmydata1 are not](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=984791)
* [#984792 eatmydata-udeb: finish-install.d/13eatmydata-udeb executes earlier than 60remove-live-packages on live ISO](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=984792)
* [#984794 calamares: use eatmydata to speed up last installation step (60remove-live-packages)](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=984794)
